import { connect } from 'react-redux';
import { Feed } from '../../components/feed/feed';
import { logoutUser } from '../../actions/session/actions';

const mapStateToProps = state => ({
  routes: state.routes,
  user: state.sessionReducer.user,
});

const mapDispatchToProps = {
  logout: logoutUser,
};

export default connect(mapStateToProps, mapDispatchToProps)(Feed);
