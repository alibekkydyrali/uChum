import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyCKAq1qWL6heXbD_5ZQePoIBSUELAj4Zf4",
  authDomain: "uchum-test.firebaseapp.com",
  databaseURL: "https://uchum-test.firebaseio.com",
  projectId: "uchum-test",
  storageBucket: "uchum-test.appspot.com",
  messagingSenderId: "937542529926"
}

let instance = null;

class FirebaseService {
  constructor() {
    if (!instance) {
      this.app = firebase.initializeApp(config);
      instance = this;
    }
    return instance;
  }
}

const firebaseService = new FirebaseService().app;
export default firebaseService;


