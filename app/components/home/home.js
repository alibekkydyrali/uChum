import React, { Component } from 'react';
import { View, Image, Text, Dimensions, TouchableOpacity } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
import { styles } from './styles';
import { Actions } from 'react-native-router-flux';
import { LoadingIndicator } from '../loadingIndicator/loadingIndicator';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';
import { Container, Header, Content, Card, CardItem, Thumbnail, Icon, Left, Body, Right, Button } from 'native-base';
import { StackNavigator, TabBarBottom } from 'react-navigation';
import * as firebase from 'firebase';

const data = [

]

export class Home extends React.Component {

  static navigationOptions = {
    tabBarIcon:({ tintColor }) =>(<Icon name='ios-home'  style={{ color: tintColor}}  />  ),
    headerRight:(
      <TouchableOpacity >
         <Icon name = "md-reorder"  style={{fontSize: 35, color: 'white'}} /> 
     </TouchableOpacity>
    )
    
}


  constructor(props) {
    super(props);
    this.state = {
    }
  }

  logout() {
    this.props.logout();
    setTimeout(() => {
      Actions.reset('login');
    }, 100);
  }
  
  componentDidMount(){
    var recentPostsRef = firebase.database().ref('/flat');
    recentPostsRef.once('value').then(snapshot => {
    data.push(snapshot.val())
    console.log(data)
})
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={{flex:1}}>

        <IndicatorViewPager
            
            style={{flex:1, backgroundColor:'white', flexDirection:'column-reverse'}}
            indicator={this._renderTitleIndicator()}
        >
            <View style={{backgroundColor:'#e5e5e5'}}>
              <Container>
                <Content>
                  <Card>
                    <CardItem>
                      <Left>
                        <Thumbnail source={{uri: 'https://i1.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100'}} style={{height: 45}} />
                      <Body>
                        <Text>Kydyrali Alibek</Text>
                      </Body>
                      </Left>
                    </CardItem>
                <CardItem cardBody>
              <Image source={{uri: 'https://3m20wtf9vk-flywheel.netdna-ssl.com/wp-content/uploads/2013/04/Interior-design.jpg'}} style={{height: 250, width: null, flex: 1}}/>
            </CardItem>
            <CardItem style={{height: 60}}>
              <Left alignItems='center'>
                <Button  transparent title="" onPress={() => this._handlePress1()}>
                  <Icon name="cash" /> 
                  <Text style={{marginLeft:5}}>300$</Text>
                </Button>
              </Left>
            
              
            </CardItem>
          </Card>
          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={{uri: 'https://i1.wp.com/www.winhelponline.com/blog/wp-content/uploads/2017/12/user.png?resize=256%2C256&quality=100'}} style={{height: 45}} />
                <Body>
                  <Text>Kissu</Text>
                </Body>
              </Left>
            </CardItem>
            <CardItem cardBody>
              <Image source={{uri: 'https://media.ied.it/ied.edu/course/header-ID-FI.jpg'}} style={{height: 250, width: null, flex: 1}}/>
            </CardItem>
            <CardItem style={{height: 60}}>
              <Left alignItems='center'>
                <Button transparent title="" onPress={() => this._handlePress1()}>
                  <Icon name="cash" />
                  <Text style={{marginLeft:5}}>320$</Text>
                </Button>
              </Left>
             
              
            </CardItem>
          </Card>
        </Content>
      </Container>    
            </View>
            <View style={{backgroundColor:'#e5e5e5'}}>
                
            </View>
          
        </IndicatorViewPager>
        
        <View style={{position:'absolute', bottom:5, right:5}}> 
          <Button  transparent title="" onPress={Actions.add}>
            <Icon name="md-add-circle"  style={{fontSize: 35, color: '#2299ec'}} /> 
          </Button>
        </View>
        
    </View>
    );
  }
_renderTitleIndicator() {
    return <PagerTitleIndicator titles={['Have flat', 'Need flat']} itemStyle={{width:Dimensions.get('window').width/2}} selectedItemStyle={{width:Dimensions.get('window').width/2}} selectedItemTextStyle={{color:'#2e99e4'}} selectedBorderStyle={{backgroundColor:'#2e99e4'}} itemTextStyle={{color:'#000'}} />;
}

_handlePress1() {
  console.log('Pressed price!');
}
_handlePress() {
  console.log('Pressed!');
}


}


