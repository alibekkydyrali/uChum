import React, { Component } from 'react';
import { View, Image, Text, Dimensions, TouchableOpacity, Button, ScrollView, Alert, TouchableWithoutFeedback , Keyboard } from 'react-native';
// import Icon from 'react-native-vector-icons/FontAwesome';
import { styles } from './styles';
import { Actions } from 'react-native-router-flux';
import { LoadingIndicator } from '../loadingIndicator/loadingIndicator';
import {PagerTabIndicator, IndicatorViewPager, PagerTitleIndicator, PagerDotIndicator} from 'rn-viewpager';
import { Container, Header, Content, Card, CardItem, Thumbnail, Icon, Left, Body, Right, Form, Item,Label, Input } from 'native-base';
import { ImagePicker, Permissions, Constants } from 'expo';
import * as firebase from 'firebase';
import firebaseService from '../../enviroments/firebase';
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';

export class Profile extends React.Component {
  
  state = {
    result: null,
  };
  
  static navigationOptions = {
    tabBarIcon:({ tintColor }) =>(<Icon name="md-contact" style={{ color: tintColor}}  /> ),
    headerRight:(
      <TouchableOpacity onPress={()=>this.submitprofile()}>
         <Icon name="ios-checkmark-circle-outline"  style={{fontSize: 35, color: 'white'}} /> 
     </TouchableOpacity>
)
  }

  constructor(props) {
    super(props);
    this.state = {fullName: '', phone: '', date: '', gender: ''};
  }




  logout() {
    this.props.logout();
    setTimeout(() => {
      Actions.reset('login');
    }, 100);
  }

  
  onChooseImagePress = async () => {
    //let result = await ImagePicker.launchCameraAsync();
    const permissions = Permissions.CAMERA_ROLL;
    const { status } = await Permissions.askAsync(permissions);
    let result = await ImagePicker.launchImageLibraryAsync(
      {
        allowsEditing: true,
        aspect: [4, 3],
        base64: false,
      }
    );

    if (!result.cancelled) {
        var url = result.uri;
        url1 = url.indexOf('/ImagePicker/');
        url2 = url.substr(url1);
      this.uploadImage(result.uri, url2)
        .then(() => {
          Alert.alert("Success");
        })
        .catch((error) => {
          Alert.alert(error);
        });
        
      console.log(url2);
    }
  }

  uploadImage = async (uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob();

    var ref = firebase.storage().ref().child("profileImage/" + imageName);
    return ref.put(blob);
  }


  updateValues(text, field){
    if(field=='fullName'){
      this.setState({fullName:text})
    }
    else if(field=='phone'){
      this.setState({phone:text})
    }
    else if(field=='date'){
      this.setState({date:text})
    }
    else if(field=='gender'){
      this.setState({gender:text})
    }
    
  }

    submitprofile(){
    firebase.auth().onAuthStateChanged(user => {
          if(user) {
              firebase.database().ref('user/' + user.uid).set({
              fullName: this.state.fullName,
              phone: this.state.phone,
              gender: this.state.gender,
              date: this.state.date
            });
            
          }
        })

  }

  
  render() {

    let data = [{
      value: 'Any',
    }, {
      value: 'Male',
    }, {
      value: 'Female',
    }];
    let { image } = this.state;
    const deviceWidth = Dimensions.get('window').width;
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
      <View style={{flex:1}}>

        <IndicatorViewPager
            style={{flex:1, backgroundColor:'white', flexDirection:'column-reverse'}}
            indicator={this._renderTitleIndicator()}
            >
          <View style={{backgroundColor:'#e5e5e5'}}>
            <Container style={{justifyContent:'flex-end', alignItems:'center'}}>
              <ScrollView style={{flex: 1}}>
                
                <View style = {{alignItems: 'center', justifyContent: 'center'}}>
                  
                  <View style={{width:80, height:80, borderRadius: 40}}>
                    <Image resizeMode='contain' style={{width:80, height:80}} source = {require('../../../assets/icons/firebase.png')}/> 
                  </View>
                  
                  <TouchableOpacity 
                    onPress={this.onChooseImagePress} style={{marginTop:5}}>
                    <Text style={{color:'#2e99e4', fontSize:15}}>Choose photo</Text>
                  </TouchableOpacity>
                </View>

          <Form style={styles.form}>
            <Item floatingLabel >
            <Label >Name</Label>
            <Input
              keyboardType='email-address'
              returnKeyType='next'
              autoCorrect={false}
              onChangeText={(fullName) => this.updateValues(fullName, 'fullName')}
              style={{ color: '#333' }}
            />
            </Item>

            <Item floatingLabel >
            <Label style={{width:deviceWidth}}>Phone</Label>
              <Input
                autoCorrect={false}
                autoCapitalize="none"
                onChangeText={(phone) => this.updateValues(phone, 'phone')}
                style={{ color: '#333'}}
              />
            </Item>
            
            <Item >
            <Label>Birthday</Label>
            <DatePicker
              style={{width:200, marginTop:10}}
              date={this.state.date}
              mode="date"
              placeholder="select date"
              format="DD-MM-YYYY"
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              // showIcon = { false }
              // Text = 'Birthday'
              customStyles={{
                dateIcon: {
                  position: 'absolute',
                  left: 0,
                  top: 4,
                  marginLeft: 0
                },
                dateInput: {
                  marginLeft: 36,
                  flexGrow: 1,
                  borderWidth: 0
                }
                // ... You can check the source to find the other keys.
          }}
              onDateChange={(date) => this.updateValues(date,'date')}
            />
            </Item>
            
            <Dropdown
              label='Gender'
              data={data}
              containerStyle={{ width:deviceWidth, paddingLeft:deviceWidth*0.04}}
              onChangeText = {(gender) => this.updateValues(gender, 'gender')}
              />
            {/* <View><Button title='asd' onPress={()=>this.submitprofile()} /></View> */}
          </Form>

          <TouchableOpacity  
            onPress={this.logout.bind(this)} 
            style={{
              alignItems:'center',
              justifyContent:'center',
              width:deviceWidth*0.8,
              height:40,
              borderRadius:26,
              backgroundColor:'#FF4081',
              marginLeft:deviceWidth*0.1
              }} >
            <Text style={{color:'white', fontSize:15}}>Logout</Text>
          </TouchableOpacity>

              </ScrollView>
            </Container>    
          </View>
          
          <View style={{backgroundColor:'#e5e5e5'}}>
           
          </View>
          
        </IndicatorViewPager>



      
       
        
      </View>
      </TouchableWithoutFeedback> 
    );
  }
_renderTitleIndicator() {
    return <PagerTitleIndicator 
              titles={['Basic', 'Preferences']}
              itemStyle={{width:Dimensions.get('window').width/2}}
              selectedItemStyle={{width:Dimensions.get('window').width/2}}
              selectedItemTextStyle={{color:'#2e99e4'}}
              selectedBorderStyle={{backgroundColor:'#2e99e4'}}
              itemTextStyle={{color:'#000'}} 
              trackScroll={true}
              />;
}

}


