import { StyleSheet, Dimensions } from 'react-native';
const deviceWidth = Dimensions.get('window').width;
export const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: '#e5e5e5',
  },
  image: {
    width: 100,
    height: 100,
  },
  primary: {
    color: 'rgb(116, 70, 195)'
  },
  marginBox: {
    alignItems: 'center',
    margin: 20
  },
  title: {
    fontSize: 24,
    margin: 20
  },
  form:{
    flex: 1,
    marginBottom:40,
    width:deviceWidth
  },
});
