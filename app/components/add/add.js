import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TouchableWithoutFeedback, Keyboard, Image, FlatList, Button, Dimensions } from 'react-native';
import * as firebase from 'firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
// import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Container, Header, Content, Form, Item, Input, Label } from 'native-base';
import { Constants, Permissions, ImagePicker } from 'expo';
import { Card } from "react-native-elements";


const data = [
  
    "file:///data/user/0/host.exp.exponent/cache/ExperienceData/%2540anonymous%252F272018-0a17a4ad-851a-4f98-8d6b-a434c878312b/ImagePicker/a6870eb9-bae6-41c4-b6b8-d96c956e1d77.jpg"

  ];

export default class Add extends React.Component{
    // static navigationOptions = {header: n };

    // state = {
    //     image: null,
    //   };

      constructor(props) {
        super(props);
        this.state = {
          data: data,
          price: '', 
          adress: '', 
          description: '',
          image: '', 
        }
        };
      

        onChooseImagePress = async () => {
            //let result = await ImagePicker.launchCameraAsync();
            const permissions = Permissions.CAMERA_ROLL;
            const { status } = await Permissions.askAsync(permissions);
            let result = await ImagePicker.launchImageLibraryAsync(
              {
                allowsEditing: true,
                aspect: [4, 3],
                base64: false,
              }
            );
        
            if (!result.cancelled) {
                var url = result.uri;
                url1 = url.indexOf('/ImagePicker/');
                url2 = url.substr(url1);
                this.setState({image:url2})
                data.push(result.uri);
                this.uploadImage(result.uri, url2)
                .then(() => {
                  Alert.alert("Success");
                })
                .catch((error) => {
                  Alert.alert(error);
                });
                
              console.log(url2);
            }
          }
        
          uploadImage = async (uri, imageName) => {
            const response = await fetch(uri);
            const blob = await response.blob();
        
            var ref = firebase.storage().ref().child("addImage/" + imageName);
            return ref.put(blob);
          }
        
    



      submitad(){
        firebase.auth().onAuthStateChanged(user => {
              if(user) {
                  firebase.database().ref('flat/' + user.uid).set({
                  price: this.state.price,
                  adress: this.state.adress,
                  description: this.state.description,
                  image: this.state.image
                });
                
              }
            })
    
      }

      updateValues(text, field){
        if(field=='price'){
          this.setState({price:text})
        }
        else if(field=='adress'){
          this.setState({adress:text})
        }
        else if(field=='description'){
          this.setState({description:text})
        }
        
      }



    render(){
        const deviceWidth = Dimensions.get('window').width;
        return(
            <Container>
                <Content>
                    <Form>
                        <View style ={{
                            alignItems: 'center',
                            height: 125,
                            flexDirection: 'row',
                        }}>

                            <TouchableOpacity onPress={this.onChooseImagePress} style = {{
                                            width: 100,
                                            height: 100,
                                            borderWidth: 1,
                                            borderColor: 'red',
                                            borderRadius: 15,
                                            alignItems: 'center',
                                            justifyContent: 'center',
                                        }}>
                                <Text>+</Text>
                            </TouchableOpacity>
                            <FlatList
                                horizontal
                                data={this.state.data}
                                extraData={this.state.data}
                                renderItem={({ item: rowData }) => (
                                <View>
                        
                                    {rowData &&
                                <Image source={{ uri: rowData }} style={{ padding: 0, width: 100, height: 100, margin: 2, borderRadius: 10}} />}
                                    </View>
                                )}
                                
                                keyExtractor={(item, index) => index}
                            />
                            
                        </View>
                        <Item floatingLabel>
                            <Label>Price</Label>
                            <Input 
                            keyboardType = 'numeric'
                            onChangeText={(price) => this.updateValues(price, 'price')}
                            />
                        </Item>

                        <Item floatingLabel>
                            <Label>Adress</Label>
                            <Input onChangeText={(adress) => this.updateValues(adress, 'adress')}/>
                        </Item>
                        
                        <Item floatingLabel last>
                            <Label>Description</Label>
                            <Input 
                            multiline = {true} numberOfLines = {5}
                            onChangeText={(description) => this.updateValues(description, 'description')}
                            />
                        </Item>
                        <TouchableOpacity 
                            onPress={()=>this.submitad()}
                            style = {{
                                alignItems:'center',
                                justifyContent:'center',
                                width:deviceWidth*0.8,
                                height:40,
                                borderRadius:26,
                                backgroundColor:'#FF4081',
                                marginLeft:deviceWidth*0.1,
                                marginTop: 10,
                                
                            }}>
                            <Text style = {{color: 'white'}}> Submit </Text>
                        </TouchableOpacity>
                    </Form>
                </Content>
            </Container>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});