import React, {Component} from 'react';
import { Text, View, Image } from 'react-native';
import { styles } from './styles';
import { Scene } from 'react-native-router-flux';
import { connect, Provider } from 'react-redux';
import ProfileContainer from '../../containers/profile/profileContainer';
import HomeContainer from '../../containers/home/homeContainer';
import SessionContainer from '../../containers/session/sessionContainer';
import SignupContainer from '../../containers/session/signupContainer';
import FeedContainer from '../../containers/feed/feedContainer';
import { RouterRedux } from '../../containers/routes/routesContainer';
import { configureStore } from '../../store/store';
import home from '../home/home';
import profile from '../profile/profile';
import feed from '../feed/feed';
import {TabNavigator, TabBarBottom} from 'react-navigation';
import Icon from 'react-native-vector-icons/Ionicons';
import Add from '../add/add';


/*var MainScreenNavigator = TabNavigator({
  Home:{screen:home},
  Profile:{screen:profile}
}); 
MainScreenNavigator.navigationOptions = {
  title:"Tab1"
}

export default MainScreenNavigator;*/

const store = configureStore();
const TabIcon = ({ selected, title }) => {
  return (
    <Text style={{color: selected ? 'red' :'black'}}>{title}</Text>
  );
}

export class Routes extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <RouterRedux navigationBarStyle={styles.navBar} tintColor='#ffffff' titleStyle={styles.barButtonTextStyle}>
          <Scene key="root">
            <Scene key="login" component={SessionContainer} title="Login" initial={true}/>
            <Scene key="signup" component={SignupContainer} title="Signup"/>
            <Scene key="add" component={Add} title="Flat"/>
            <Scene key="home" 
                   tabs={true}
                   tabBarStyle={{backgroundColor:'#FFFFFF'}} 
                   tabBarPosition={'bottom'}
                   tabBarComponent={TabBarBottom}
                   hideNavBar={true}
                   renderTitle={() => (
                    <View>
                      <Image style={styles.image} source = {require('../../../assets/icons/uchum.png')} />
                    </View>
                  )}
            >
            <Scene
              key="home"
              component={HomeContainer}
              title="Home"
            >
              
            </Scene>
            <Scene
              key="feed"
              component={FeedContainer}
              title="Feed"
            >
            </Scene>
            <Scene
              key="profile"
              component={ProfileContainer}
              title="Profile"
            >
            </Scene>
           
           </Scene>
          </Scene>
        </RouterRedux>
      </ Provider>
    );
  }
}
